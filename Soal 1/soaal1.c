#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>

void downloadFile()
{
    pid_t pid = fork();

    if (pid == -1) {
        exit(EXIT_FAILURE);
    }

    if (pid == 0) {
        execl("/usr/bin/wget", "wget", "-q", "-O", "binatang.zip", "https://drive.google.com/uc?export=download&id=1oDgj5kSiDO0tlyS7-20uz7t20X3atwrq", NULL);
        exit(EXIT_FAILURE);
    }

    int status;
    waitpid(pid, &status, 0);

    if (!WIFEXITED(status)) {
        exit(EXIT_FAILURE);
    }
}

void unzipFile()
{
    pid_t pid = fork();

    if (pid == -1) {
        exit(EXIT_FAILURE);
    }

    if (pid == 0) {
        execl("/usr/bin/unzip", "unzip", "-q", "binatang.zip", NULL);
        exit(EXIT_FAILURE);
    }

    int status;
    waitpid(pid, &status, 0);

    if (!WIFEXITED(status)) {
        exit(EXIT_FAILURE);
    }

    system("rm binatang.zip");
}

void jagaHewan()
{
    FILE* fp;
    char c, namaHewan[50];
    int i = 0;

    system("ls | grep -e '^[[:alpha:]]\\+_.*\\.jpg$' | shuf -n 1 > temp.txt");

    fp = fopen("temp.txt", "r");
    if (fp == NULL) {
        exit(EXIT_FAILURE);
    }

    while ((c = fgetc(fp)) != EOF && c != '_') {
        namaHewan[i++] = c;
    }
    namaHewan[i] = '\0';

    fclose(fp);
    system("rm temp.txt");

    printf("Hewan yang mendapat shift untuk dijaga :  %s\n", namaHewan);
}

void zip(char* dir)
{
    pid_t pid = fork();

    if (pid == -1) {
        exit(EXIT_FAILURE);
    }

    if (pid == 0) {
        char zipName[100];
        sprintf(zipName, "%s.zip", dir);

        execl("/usr/bin/zip", "zip", "-q", "-r", "-m", zipName, dir, NULL);
        exit(EXIT_FAILURE);
    }

    int status;
    waitpid(pid, &status, 0);

    if (!WIFEXITED(status)) {
        printf("Proses zip tidak berhenti dengan normal\n", dir);
        exit(EXIT_FAILURE);
    }
}

int main()
{
    //melakukan download dan zip
    downloadFile();
    unzipFile();

    //melihat hewan yang akan dijaga
    jagaHewan();

    //memindahkan hewan sesuai t4 tinggal
    system("mkdir HewanDarat HewanAmphibi HewanAir");
    system("mv *darat.jpg HewanDarat");
    system("mv *amphibi.jpg HewanAmphibi");
    system("mv *air.jpg HewanAir");
    
    //zip direktori
    system("zip -r HewanDarat.zip HewanDarat");
    system("zip -r HewanAmphibi.zip HewanAmphibi");
    system("zip -r HewanAir.zip HewanAir");

    //menghapus direktori
    system("rm -r HewanDarat");
    system("rm -r HewanAmphibi");
    system("rm -r HewanAir");
    
    return 0;
}
