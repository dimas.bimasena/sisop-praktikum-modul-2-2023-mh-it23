# Laporan Resmi Modul 2 Sistem Operasi
Adimasdefatra Bimsasena 5027211040

# Soal 1

Code ini adalah program dalam bahasa pemrograman C yang berfungsi untuk melakukan pengunduhan file dari internet, mengekstrak isi file tersebut, dan memproses gambar-gambar hewan untuk kemudian dikelompokkan ke dalam tiga kategori (hewan darat, hewan air, dan hewan amfibi) berdasarkan nama file gambar tersebut. Setelah itu, program akan membuat file zip untuk masing-masing kategori hewan dan menghapus direktori yang digunakan untuk memproses gambar.

Berikut adalah penjelasan singkat dari setiap fungsi dalam program:

downloadFile() : Fungsi ini menggunakan perintah fork() untuk membuat proses baru. Proses baru tersebut kemudian menjalankan perintah execl() untuk mengunduh file dari internet menggunakan utilitas wget. Setelah unduhan selesai, proses akan keluar menggunakan exit().
unzipFile() : Fungsi ini juga menggunakan fork() untuk membuat proses baru. Proses baru tersebut kemudian menjalankan perintah execl() untuk mengekstrak isi file yang telah diunduh menggunakan utilitas unzip. Setelah ekstraksi selesai, proses akan keluar menggunakan exit().
jagaHewan() : Fungsi ini menggunakan perintah system() untuk menjalankan perintah shell yang mencari file gambar hewan dengan pola nama tertentu, dan kemudian memilih satu file secara acak. Nama hewan yang dipilih kemudian dicetak ke layar.
zip() : Fungsi ini juga menggunakan fork() untuk membuat proses baru. Proses baru tersebut kemudian menjalankan perintah execl() untuk membuat file zip dari suatu direktori menggunakan utilitas zip. Setelah file zip berhasil dibuat, direktori yang digunakan untuk membuat file zip tersebut akan dihapus menggunakan perintah rm -r.
Setelah fungsi-fungsi tersebut didefinisikan, program utama main() kemudian memanggil fungsi-fungsi tersebut secara berurutan untuk melakukan pengunduhan file, mengekstrak isi file, memproses gambar-gambar hewan, membuat file zip, dan menghapus direktori. Program kemudian akan keluar dengan nilai 0 jika semua operasi berjalan dengan sukses.


#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>

void downloadFile()
{
    pid_t pid = fork();

    if (pid == -1) {
        exit(EXIT_FAILURE);
    }

    if (pid == 0) {
        execl("/usr/bin/wget", "wget", "-q", "-O", "binatang.zip", "https://drive.google.com/uc?export=download&id=1oDgj5kSiDO0tlyS7-20uz7t20X3atwrq", NULL);
        exit(EXIT_FAILURE);
    }

    int status;
    waitpid(pid, &status, 0);

    if (!WIFEXITED(status)) {
        exit(EXIT_FAILURE);
    }
}

void unzipFile()
{
    pid_t pid = fork();

    if (pid == -1) {
        exit(EXIT_FAILURE);
    }

    if (pid == 0) {
        execl("/usr/bin/unzip", "unzip", "-q", "binatang.zip", NULL);
        exit(EXIT_FAILURE);
    }

    int status;
    waitpid(pid, &status, 0);

    if (!WIFEXITED(status)) {
        exit(EXIT_FAILURE);
    }

    system("rm binatang.zip");
}

void jagaHewan()
{
    FILE* fp;
    char c, namaHewan[50];
    int i = 0;

    system("ls | grep -e '^[[:alpha:]]\\+_.*\\.jpg$' | shuf -n 1 > temp.txt");

    fp = fopen("temp.txt", "r");
    if (fp == NULL) {
        exit(EXIT_FAILURE);
    }

    while ((c = fgetc(fp)) != EOF && c != '_') {
        namaHewan[i++] = c;
    }
    namaHewan[i] = '\0';

    fclose(fp);
    system("rm temp.txt");

    printf("Hewan yang mendapat shift untuk dijaga :  %s\n", namaHewan);
}

void zip(char* dir)
{
    pid_t pid = fork();

    if (pid == -1) {
        exit(EXIT_FAILURE);
    }

    if (pid == 0) {
        char zipName[100];
        sprintf(zipName, "%s.zip", dir);

        execl("/usr/bin/zip", "zip", "-q", "-r", "-m", zipName, dir, NULL);
        exit(EXIT_FAILURE);
    }

    int status;
    waitpid(pid, &status, 0);

    if (!WIFEXITED(status)) {
        printf("Proses zip tidak berhenti dengan normal\n", dir);
        exit(EXIT_FAILURE);
    }
}

int main()
{
    //melakukan download dan zip
    downloadFile();
    unzipFile();

    //melihat hewan yang akan dijaga
    jagaHewan();

    //memindahkan hewan sesuai t4 tinggal
    system("mkdir HewanDarat HewanAmphibi HewanAir");
    system("mv *darat.jpg HewanDarat");
    system("mv *amphibi.jpg HewanAmphibi");
    system("mv *air.jpg HewanAir");
    
    //zip direktori
    system("zip -r HewanDarat.zip HewanDarat");
    system("zip -r HewanAmphibi.zip HewanAmphibi");
    system("zip -r HewanAir.zip HewanAir");

    //menghapus direktori
    system("rm -r HewanDarat");
    system("rm -r HewanAmphibi");
    system("rm -r HewanAir");
    
    return 0;
}


# Soal 2

Program ini adalah sebuah program C yang membuat folder dengan nama timestamp, mendownload 15 gambar dengan nama file berisi timestamp dan nomor urut, dan kemudian mengompres folder tersebut menjadi file zip. Selain itu, program ini juga membuat file shell script "killer.sh" yang berguna untuk menghentikan program dengan mengirimkan sinyal SIGINT atau SIGTERM ke proses utama.

Pertama, program ini mendefinisikan beberapa library yang digunakan, seperti stdio.h, unistd.h, time.h, sys/types.h, sys/wait.h, dirent.h, string.h, dan signal.h.

Setelah itu, program ini mendefinisikan beberapa fungsi, yaitu:

create_folder: Fungsi ini membuat folder dengan nama yang diberikan sebagai argumen. Fungsi ini melakukan fork dan menjalankan perintah "mkdir" pada child process.
download_images: Fungsi ini mendownload 15 gambar dari situs picsum.photos dengan ukuran yang bervariasi. Setiap gambar diberi nama yang berisi timestamp dan nomor urut. Setelah semua gambar berhasil didownload, fungsi ini mengompres folder tersebut menjadi file zip dan menghapus folder yang sudah di-zip. Fungsi ini menggunakan fork untuk menjalankan perintah "wget", "zip", dan "rm" pada child process.
create_killer: Fungsi ini membuat file shell script "killer.sh" yang berguna untuk menghentikan program dengan mengirimkan sinyal SIGINT atau SIGTERM ke proses utama. Fungsi ini juga mengubah permission dari file "killer.sh" agar bisa dijalankan. Fungsi ini menggunakan fork untuk menjalankan perintah "echo" dan "chmod" pada child process.
handle_sigterm: Fungsi ini akan dipanggil ketika program menerima sinyal SIGTERM. Fungsi ini hanya menampilkan pesan "Exiting..." dan keluar dari program.

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <dirent.h>
#include <string.h>
#include <signal.h>

void create_folder(char *path) {
    pid_t pid = fork();
    if (pid == 0) {
        char *argv[] = {"mkdir", path, NULL};
        execv("/bin/mkdir", argv);
    }
    sleep(1);
}

void download_images(char *path) {
    int i;
    time_t now = time(NULL);
    struct tm *t = localtime(&now);
    char timestamp[20];
    strftime(timestamp, sizeof(timestamp), "%Y-%m-%d_%H:%M:%S", t);

    for (i = 0; i < 15; i++) {
        now = time(NULL);
        t = localtime(&now);
        char filename[50];
        sprintf(filename, "%s/%s_%d.jpg", path, timestamp, i+1);

        char url[50];
        int size = (now % 1000) + 50;
        sprintf(url, "https://picsum.photos/%d", size);

        pid_t pid = fork();
        if (pid == 0) {
            char *argv[] = {"wget", "-q", "-O", filename, url, NULL};
            execv("/usr/bin/wget", argv);
        }
        sleep(5);
    }

    char zip_file[50];
    sprintf(zip_file, "%s.zip", path);
    pid_t pid = fork();
    if (pid == 0) {
        char *argv[] = {"zip", "-qr", zip_file, path, NULL};
        execv("/usr/bin/zip", argv);
    }

    int status;
    waitpid(pid, &status, 0);

    pid = fork();
    if (pid == 0) {
        char *argv[] = {"rm", "-rf", path, NULL};
        execv("/bin/rm", argv);
    }
}

void create_killer(char *mode, char *path) {
    char killer_path[50];
    sprintf(killer_path, "%s/killer.sh", path);

    FILE *fptr = fopen(killer_path, "w");
    if (strcmp(mode, "-a") == 0) {
        fprintf(fptr, "#!/bin/bash\nkillall -SIGINT main\nrm -rf %s", path);
    } else if (strcmp(mode, "-b") == 0) {
        fprintf(fptr, "#!/bin/bash\nkillall -SIGTERM main\nrm %s/killer.sh", path);
    }
    fclose(fptr);

    pid_t pid = fork();
    if (pid == 0) {
        char *argv[] = {"chmod", "+x", killer_path, NULL};
        execv("/bin/chmod", argv);
    }
    wait(NULL);
}

void handle_sigterm(int sig) {
    printf("Exiting...\n");
    exit(0);
}

int main(int argc, char *argv[]) {
    char mode[3];
    if (argc == 2) {
        strcpy(mode, argv[1]);
    } else {
        printf("Usage: ./main [-a/-b]\n");
        exit(0);
    }

    if (strcmp(mode, "-a") != 0 && strcmp(mode, "-b") != 0) {
        printf("Invalid argument. Usage: ./main [-a/-b]\n");
        exit(0);
    }
signal(SIGTERM, handle_sigterm);

while (1) {
    time_t now = time(NULL);
    struct tm *t = localtime(&now);
    char foldername[20];
    strftime(foldername, sizeof(foldername), "%Y-%m-%d_%H:%M:%S", t);

    create_folder(foldername);
    download_images(foldername);
    create_killer(mode, foldername);

    sleep(30);
}

return 0;
}

# Soal 3

Kode yang diberikan ditulis dalam bahasa C dan melakukan tugas-tugas berikut:

Unduh dan ekstrak file zip bernama players.zip menggunakan perintah wget dan unzip.
Hapus semua file di direktori pemain yang tidak mengandung string _ManUtd_ dalam nama filenya.
Kategorikan file yang tersisa di direktori pemain berdasarkan jenisnya (kiper, bek, gelandang, atau striker), dan pindahkan ke direktori masing-masing.
Buat formasi tim sepak bola dengan memilih pemain dari direktori pemain dan menulis nama pemain yang dipilih ke file teks. Formasi tim terdiri dari satu penjaga gawang, beberapa bek, gelandang, dan penyerang, dengan jumlah pemain 11 orang. Formasi yang mungkin adalah 4-4-2, 4-3-3, dan 3-5-2 .
Program ini menggunakan berbagai pemanggilan sistem seperti fork(), execlp(), mkdir(), waitpid(), opendir(), readdir(), strcmp(), dan strstr(), antara lain. Kode ini juga menggunakan beberapa fungsi library C seperti sprintf(), snprintf(), fprintf(), fopen(), dan fclose() untuk memanipulasi string dan file.


#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <dirent.h>
#include <string.h>

#define max_a 50 
#define max_b 100


void download(){
// download and extract players.zip
    pid_t pid = fork();
    if (pid == 0) {
        execlp("wget", "wget", "-O", "players.zip", "https://drive.google.com/u/0/uc?id=1zEAneJ1-0sOgt13R1gL4i1ONWfKAtwBF&export=download", NULL);
    } else {
        waitpid(pid, NULL, 0);
        pid_t pid2 = fork();
        if (pid2 == 0) {
            execlp("unzip", "unzip", "players.zip", NULL);
        } else {
            waitpid(pid2, NULL, 0);
            // delete zip file
            pid_t pid3 = fork();
            if (pid3 == 0) {
                execlp("rm", "rm", "players.zip", NULL);
            } else {
                waitpid(pid3, NULL, 0);
            }
        }
    }
}

void rmExceptMU () {
    DIR *dir;
    struct dirent *ent;
    char cmd[300];
    chdir("players");
    dir = opendir(".");
    if (dir != NULL) {
        while ((ent = readdir(dir)) != NULL) {
            if (strcmp(ent->d_name, ".") != 0 && strcmp(ent->d_name, "..") != 0) {
                if (strstr(ent->d_name, "_ManUtd_") == NULL) {
                    sprintf(cmd, "rm %s", ent->d_name);
                    pid_t pid = fork();
                    if (pid == 0) {
                        execlp("rm", "rm", ent->d_name, NULL);
                    } else {
                        waitpid(pid, NULL, 0);
                    }
                }
            }
        }
    }
}

void category(){
    DIR *dir;
    struct dirent *ent;
    chdir("players");
    dir = opendir(".");
    if (dir != NULL) {
        while ((ent = readdir(dir)) != NULL) {
            if (strcmp(ent->d_name, ".") != 0 && strcmp(ent->d_name, "..") != 0) {
                char *player_type = NULL;
                if (strstr(ent->d_name, "Kiper") != NULL) {
                    player_type = "Kiper";
                } else if (strstr(ent->d_name, "Bek") != NULL) {
                    player_type = "Bek";
                } else if (strstr(ent->d_name, "Gelandang") != NULL) {
                    player_type = "Gelandang";
                } else if (strstr(ent->d_name, "Penyerang") != NULL) {
                    player_type = "Penyerang";
                }
                if (player_type != NULL) {
                    pid_t pid = fork();
                    if (pid == 0) {
                    	execlp("mkdir", "mkdir", "player_type", NULL);
                        mkdir(player_type, 0777);
                        char dir_path[300];
                        snprintf(dir_path, 300, "./%s", player_type);
                        char file_path[300];
                        snprintf(file_path, 300, "./%s/%s", player_type, ent->d_name);
                        execlp("mv", "mv", ent->d_name, file_path, NULL);
                        exit(0);
                    } else if (pid > 0) {
                        wait(NULL);
                    } else {
                        perror("Fork failed");
                        exit(1);
                    }
                }
            }
        }
        closedir(dir);
    } else {
        printf("Cannot open directory\n");
        exit(1);
    }
}

char kiper[max_a][max_b];
char bek[max_a][max_b];
char gelandang[max_a][max_b];
char penyerang[max_a][max_b];

void buatTim(int nBek, int nGelandang, int nStriker) {
    if (1 + nBek + nGelandang + nStriker != 11)
        return;
    char filename[max_a];
    sprintf(filename, "Formation_%d-%d-%d.txt", nBek, nGelandang, nStriker);
    FILE* fp = fopen(filename, "w");
    fprintf(fp, "Formasi tim %d-%d-%d \n", nBek, nGelandang, nStriker);
    fprintf(fp, "\nKiper :\n");
    fprintf(fp, "%s, ", kiper[0]);
    fprintf(fp, "\nBek :\n");
    for (int i = 0; i < nBek; i++) {
        fprintf(fp, "%s, ", bek[i]);
    }
    fprintf(fp, "\nGelandang :\n");
    for (int i = 0; i < nGelandang; i++) {
        fprintf(fp, "%s, ", gelandang[i]);
    }
    fprintf(fp, "\nPenyerang :\n");
    for (int i = 0; i < nStriker; i++) {
        fprintf(fp, "%s, ", penyerang[i]);
    }
    fclose(fp);
    pid_t pid = fork();
    if(pid == 0) {
        execl("/usr/bin/mv", "mv", filename, "/home/jezz", NULL);
    }
}

int main() {
    //download zip & ekstrak
    download();
    //delete file yang bukan MU
    rmExceptMU();
    //buat folder berdasarkan kategori
    category();

    // perbandingan rating pemain
    int rating(const void* a, const void* b) {
        int lenA = strlen((char*)a);
        int lenB = strlen((char*)b);
        return strcmp((char*)a + lenA - 7, (char*)b + lenB - 7) < 0;
    }
    
    // mendapatkan pemain berdasarkan rating
    void pemain(char players[max_a][max_b], char* dir) {
        DIR* dr = opendir(dir);
        struct dirent* ent;
        int i = 0;

        while (ent = readdir(dr)) {
            if (strstr(ent->d_name, "_") != NULL)
                strcpy(players[i++], ent->d_name);
        }

        qsort(players, i, sizeof(players[0]), rating);
    }

    // mendapatkan pemain
    pemain(kiper, "./players/Kiper");
    pemain(bek, "./players/Bek");
    pemain(gelandang, "./players/Gelandang");
    pemain(penyerang, "./players/Penyerang");

    // membuat tim
    buatTim(2, 5, 3);
    return 0;

}





# Soal 4

Program ini merupakan implementasi sederhana dari "cron job" di lingkungan sistem operasi Unix dan Linux. Cron job adalah suatu tugas yang dijalankan secara otomatis pada waktu yang telah ditentukan sebelumnya, dan tugas ini dapat dijadwalkan untuk dijalankan pada waktu tertentu, baik itu setiap hari, setiap minggu, atau setiap bulan.

Berikut adalah penjelasan singkat dari setiap fungsi dalam program ini:

handle_sigint(int signal): Fungsi ini merupakan signal handler untuk sinyal SIGINT yang digunakan untuk menangani sinyal keyboard interrupt (Ctrl+C) dari pengguna. Fungsi ini akan menampilkan pesan "Cron job terminated." dan keluar dari program.
run_script(char* path_to_script): Fungsi ini membuat proses baru dengan menggunakan fork() dan kemudian menjalankan script pada path yang telah ditentukan dengan menggunakan execlp().
print_usage(): Fungsi ini menampilkan pesan tentang cara penggunaan program kepada pengguna.
print_error(): Fungsi ini menampilkan pesan error dan cara penggunaan program jika terdapat kesalahan pada argumen yang diberikan oleh pengguna.
is_valid_time(int hour, int minute, int second): Fungsi ini mengembalikan nilai 1 jika waktu yang diberikan oleh pengguna (jam, menit, detik) valid dan mengembalikan nilai 0 jika tidak valid.
parse_time(char* arg, int* dest): Fungsi ini digunakan untuk memparse waktu yang diberikan oleh pengguna (jam, menit, detik) yang berupa string menjadi integer. Fungsi ini akan mengembalikan nilai 1 jika parsing berhasil dan nilai 0 jika parsing gagal.
cron_job(int hour, int minute, int second, char* path_to_script): Fungsi ini merupakan inti dari program, yaitu melakukan looping secara terus-menerus untuk mengecek apakah waktu saat ini sama dengan waktu yang diberikan oleh pengguna. Jika waktu saat ini sama dengan waktu yang diberikan oleh pengguna, maka fungsi run_script() akan dipanggil untuk menjalankan script yang telah ditentukan oleh pengguna. Fungsi ini menggunakan fungsi sleep() untuk menghentikan program selama 1 detik sebelum memeriksa kembali waktu.
Pada fungsi main(), program akan mengecek jumlah argumen yang diberikan oleh pengguna. Jika jumlah argumen tidak sama dengan 5 (yaitu jam, menit, detik, dan path ke script), maka fungsi print_usage() akan dipanggil. Selain itu, program akan memeriksa apakah waktu yang diberikan oleh pengguna valid atau tidak dengan menggunakan fungsi is_valid_time() dan parse_time(). Jika waktu tidak valid atau parsing gagal, maka fungsi print_error() akan dipanggil. Jika semua validasi berhasil, maka fungsi cron_job() akan dipanggil untuk menjalankan cron job.


#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <time.h>
#include <string.h>
#include <unistd.h>

void handle_sigint(int signal);
void run_script(char* path_to_script);
void print_usage();
void print_error();
int is_valid_time(int hour, int minute, int second);
int parse_time(char* arg, int* dest);
void cron_job(int hour, int minute, int second, char* path_to_script);

int main(int argc, char* argv[]) {

    // Set up signal handler for SIGINT
    signal(SIGINT, handle_sigint);

    // Validate and parse arguments
    if (argc != 5) {
        print_usage();
        return 1;
    }
    int hour, minute, second;
    if (!parse_time(argv[1], &hour) ||
        !parse_time(argv[2], &minute) ||
        !parse_time(argv[3], &second) ||
        !is_valid_time(hour, minute, second)) {
        print_error();
        return 1;
    }
    char* path_to_script = argv[4];

    // Run cron job
    cron_job(hour, minute, second, path_to_script);

    return 0;
}

void handle_sigint(int signal) {
    printf("Cron job terminated.\n");
    exit(0);
}

void run_script(char* path_to_script) {
    int pid = fork();
    if (pid == 0) {
        execlp(path_to_script, path_to_script, NULL);
        exit(1);
    }
}

void print_usage() {
    printf("Usage: cron <hour> <minute> <second> <path_to_script>\n");
}

void print_error() {
    printf("Error: Invalid arguments.\n");
    print_usage();
}

int is_valid_time(int hour, int minute, int second) {
    return hour >= 0 && hour <= 23 &&
           minute >= 0 && minute <= 59 &&
           second >= 0 && second <= 59;
}

int parse_time(char* arg, int* dest) {
    char* endptr;
    int val = strtol(arg, &endptr, 10);
    if (*endptr != '\0' || val < 0) {
        return 0;
    }
    *dest = val;
    return 1;
}

void cron_job(int hour, int minute, int second, char* path_to_script) {
    while (1) {
        time_t current_time = time(NULL);
        struct tm* timeinfo = localtime(&current_time);
        if (timeinfo->tm_hour == hour &&
            timeinfo->tm_min == minute &&
            timeinfo->tm_sec == second) {
            run_script(path_to_script);
        }
        sleep(1);
    }
}



