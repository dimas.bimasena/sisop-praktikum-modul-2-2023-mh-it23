#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <dirent.h>
#include <string.h>

#define max_a 50 
#define max_b 100


void download(){
// download and extract players.zip
    pid_t pid = fork();
    if (pid == 0) {
        execlp("wget", "wget", "-O", "players.zip", "https://drive.google.com/u/0/uc?id=1zEAneJ1-0sOgt13R1gL4i1ONWfKAtwBF&export=download", NULL);
    } else {
        waitpid(pid, NULL, 0);
        pid_t pid2 = fork();
        if (pid2 == 0) {
            execlp("unzip", "unzip", "players.zip", NULL);
        } else {
            waitpid(pid2, NULL, 0);
            // delete zip file
            pid_t pid3 = fork();
            if (pid3 == 0) {
                execlp("rm", "rm", "players.zip", NULL);
            } else {
                waitpid(pid3, NULL, 0);
            }
        }
    }
}

void rmExceptMU () {
    DIR *dir;
    struct dirent *ent;
    char cmd[300];
    chdir("players");
    dir = opendir(".");
    if (dir != NULL) {
        while ((ent = readdir(dir)) != NULL) {
            if (strcmp(ent->d_name, ".") != 0 && strcmp(ent->d_name, "..") != 0) {
                if (strstr(ent->d_name, "_ManUtd_") == NULL) {
                    sprintf(cmd, "rm %s", ent->d_name);
                    pid_t pid = fork();
                    if (pid == 0) {
                        execlp("rm", "rm", ent->d_name, NULL);
                    } else {
                        waitpid(pid, NULL, 0);
                    }
                }
            }
        }
    }
}

void category(){
    DIR *dir;
    struct dirent *ent;
    chdir("players");
    dir = opendir(".");
    if (dir != NULL) {
        while ((ent = readdir(dir)) != NULL) {
            if (strcmp(ent->d_name, ".") != 0 && strcmp(ent->d_name, "..") != 0) {
                char *player_type = NULL;
                if (strstr(ent->d_name, "Kiper") != NULL) {
                    player_type = "Kiper";
                } else if (strstr(ent->d_name, "Bek") != NULL) {
                    player_type = "Bek";
                } else if (strstr(ent->d_name, "Gelandang") != NULL) {
                    player_type = "Gelandang";
                } else if (strstr(ent->d_name, "Penyerang") != NULL) {
                    player_type = "Penyerang";
                }
                if (player_type != NULL) {
                    pid_t pid = fork();
                    if (pid == 0) {
                    	execlp("mkdir", "mkdir", "player_type", NULL);
                        mkdir(player_type, 0777);
                        char dir_path[300];
                        snprintf(dir_path, 300, "./%s", player_type);
                        char file_path[300];
                        snprintf(file_path, 300, "./%s/%s", player_type, ent->d_name);
                        execlp("mv", "mv", ent->d_name, file_path, NULL);
                        exit(0);
                    } else if (pid > 0) {
                        wait(NULL);
                    } else {
                        perror("Fork failed");
                        exit(1);
                    }
                }
            }
        }
        closedir(dir);
    } else {
        printf("Cannot open directory\n");
        exit(1);
    }
}

char kiper[max_a][max_b];
char bek[max_a][max_b];
char gelandang[max_a][max_b];
char penyerang[max_a][max_b];

void buatTim(int nBek, int nGelandang, int nStriker) {
    if (1 + nBek + nGelandang + nStriker != 11)
        return;
    char filename[max_a];
    sprintf(filename, "Formation_%d-%d-%d.txt", nBek, nGelandang, nStriker);
    FILE* fp = fopen(filename, "w");
    fprintf(fp, "Formasi tim %d-%d-%d \n", nBek, nGelandang, nStriker);
    fprintf(fp, "\nKiper :\n");
    fprintf(fp, "%s, ", kiper[0]);
    fprintf(fp, "\nBek :\n");
    for (int i = 0; i < nBek; i++) {
        fprintf(fp, "%s, ", bek[i]);
    }
    fprintf(fp, "\nGelandang :\n");
    for (int i = 0; i < nGelandang; i++) {
        fprintf(fp, "%s, ", gelandang[i]);
    }
    fprintf(fp, "\nPenyerang :\n");
    for (int i = 0; i < nStriker; i++) {
        fprintf(fp, "%s, ", penyerang[i]);
    }
    fclose(fp);
    pid_t pid = fork();
    if(pid == 0) {
        execl("/usr/bin/mv", "mv", filename, "/home/jezz", NULL);
    }
}

int main() {
    //download zip & ekstrak
    download();
    //delete file yang bukan MU
    rmExceptMU();
    //buat folder berdasarkan kategori
    category();

    // perbandingan rating pemain
    int rating(const void* a, const void* b) {
        int lenA = strlen((char*)a);
        int lenB = strlen((char*)b);
        return strcmp((char*)a + lenA - 7, (char*)b + lenB - 7) < 0;
    }
    
    // mendapatkan pemain berdasarkan rating
    void pemain(char players[max_a][max_b], char* dir) {
        DIR* dr = opendir(dir);
        struct dirent* ent;
        int i = 0;

        while (ent = readdir(dr)) {
            if (strstr(ent->d_name, "_") != NULL)
                strcpy(players[i++], ent->d_name);
        }

        qsort(players, i, sizeof(players[0]), rating);
    }

    // mendapatkan pemain
    pemain(kiper, "./players/Kiper");
    pemain(bek, "./players/Bek");
    pemain(gelandang, "./players/Gelandang");
    pemain(penyerang, "./players/Penyerang");

    // membuat tim
    buatTim(2, 5, 3);
    return 0;

}
