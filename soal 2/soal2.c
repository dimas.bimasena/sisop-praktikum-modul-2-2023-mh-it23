#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <dirent.h>
#include <string.h>
#include <signal.h>

void create_folder(char *path) {
    pid_t pid = fork();
    if (pid == 0) {
        char *argv[] = {"mkdir", path, NULL};
        execv("/bin/mkdir", argv);
    }
    sleep(1);
}

void download_images(char *path) {
    int i;
    time_t now = time(NULL);
    struct tm *t = localtime(&now);
    char timestamp[20];
    strftime(timestamp, sizeof(timestamp), "%Y-%m-%d_%H:%M:%S", t);

    for (i = 0; i < 15; i++) {
        now = time(NULL);
        t = localtime(&now);
        char filename[50];
        sprintf(filename, "%s/%s_%d.jpg", path, timestamp, i+1);

        char url[50];
        int size = (now % 1000) + 50;
        sprintf(url, "https://picsum.photos/%d", size);

        pid_t pid = fork();
        if (pid == 0) {
            char *argv[] = {"wget", "-q", "-O", filename, url, NULL};
            execv("/usr/bin/wget", argv);
        }
        sleep(5);
    }

    char zip_file[50];
    sprintf(zip_file, "%s.zip", path);
    pid_t pid = fork();
    if (pid == 0) {
        char *argv[] = {"zip", "-qr", zip_file, path, NULL};
        execv("/usr/bin/zip", argv);
    }

    int status;
    waitpid(pid, &status, 0);

    pid = fork();
    if (pid == 0) {
        char *argv[] = {"rm", "-rf", path, NULL};
        execv("/bin/rm", argv);
    }
}

void create_killer(char *mode, char *path) {
    char killer_path[50];
    sprintf(killer_path, "%s/killer.sh", path);

    FILE *fptr = fopen(killer_path, "w");
    if (strcmp(mode, "-a") == 0) {
        fprintf(fptr, "#!/bin/bash\nkillall -SIGINT main\nrm -rf %s", path);
    } else if (strcmp(mode, "-b") == 0) {
        fprintf(fptr, "#!/bin/bash\nkillall -SIGTERM main\nrm %s/killer.sh", path);
    }
    fclose(fptr);

    pid_t pid = fork();
    if (pid == 0) {
        char *argv[] = {"chmod", "+x", killer_path, NULL};
        execv("/bin/chmod", argv);
    }
    wait(NULL);
}

void handle_sigterm(int sig) {
    printf("Exiting...\n");
    exit(0);
}

int main(int argc, char *argv[]) {
    char mode[3];
    if (argc == 2) {
        strcpy(mode, argv[1]);
    } else {
        printf("Usage: ./main [-a/-b]\n");
        exit(0);
    }

    if (strcmp(mode, "-a") != 0 && strcmp(mode, "-b") != 0) {
        printf("Invalid argument. Usage: ./main [-a/-b]\n");
        exit(0);
    }
signal(SIGTERM, handle_sigterm);

while (1) {
    time_t now = time(NULL);
    struct tm *t = localtime(&now);
    char foldername[20];
    strftime(foldername, sizeof(foldername), "%Y-%m-%d_%H:%M:%S", t);

    create_folder(foldername);
    download_images(foldername);
    create_killer(mode, foldername);

    sleep(30);
}

return 0;
}
